# Midterm Exam

## Total

88/100

## Break Down

1. Data Types:                  18/20
    - Compiles:                 5/5
    - Input:                    3/5
    - Data Types:               5/5
    - Results:                  5/5
2. Selection:                   20/20
    - Compiles:                 5/5
    - Selection Structure:      10/10
    - Results:                  5/5
3. Repetition:                  16/20
    - Compiles:                 5/5
    - Repetition Structure:     5/5
    - Returns:                  5/5
    - Formatting:               1/5
4. Arrays:                      20/20
    - Compiles:                 5/5
    - Array:                    5/5
    - Exit:                     5/5
    - Results:                  5/5
5. Objects:                     14/20
    - Variables:                5/5
    - Constructors:             3/5
    - Accessors and Mutators:   3/5
    - toString and equals:      3/5

## Comments

1. Good, but doesn't confirm that the input is an integer.
2. Good
3. Good, but format doesn't match and program doesn't confirm that the input is an integer.
4. Good
5. Non-default constructor and mutator do not validate `age` and `equals` does not follow UML diagram nor always compare `name` correctly.
