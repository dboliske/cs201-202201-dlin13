/*
 * Write a Java class based on the following UML diagram. 
 * Include all standard methods, such as constructors, mutators, accessors, toString, and equals. 
 * Additionally, implement any other methods shown in the diagram.
 * Donglei Lin
 */

package exams.first;

public class Pet {
	
	private String name;
	private int age;
	
	public Pet() {
		name = "Bobby";
		age = 1;
		
	}
	
	public Pet(String name, int age) {
		this.name = name;
		this.age = age;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public void setAge(int age) {
		this.age = age;
	}
	
	public String getName() {
		return name;
	}
	
	public int getAge() {
		return age;
	}
	
	public String toString() {
		return name + " is " + age + " years old.";
	}
	
	public boolean equals(Pet p) {
        if (!name.equals(p.getName()) && age == p.getAge()) {
        	System.out.println("Their name is different but same age.");
        	return false;
        } else if (age != p.getAge() && name == p.getName()) {
        	System.out.println("They have the same name but different age");
        	return false;
        }
        System.out.println("They have the same name and age.");
        return true;
    }

}
