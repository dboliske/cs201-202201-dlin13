/*
 * Write a program that prompts the user for an integer and then prints out a Triangle of that height and width. 
 * For example, if the user enters 3, then your program should print the following:
 *   * * *
 *   * *
 *   *
 *  Donglei Lin 
 */ 
 

package exams.first;

import java.util.Scanner;

public class QuestionThree {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		
		System.out.print("Enter any integer as the size of the square: ");
		
		int s = Integer.parseInt(input.nextLine());
		
		System.out.print("Enter any character of the Square: ");
		char cr  = input.nextLine().charAt(0);
		
		input.close();
		
		int i, j;
		for (i = 1; i <= s; i++)
		{
			for(j = 1; j <= +(s + 1 - i); j++)
			{
				System.out.print(cr + "");
			}
			System.out.print("\n");
		}

	}

}
