package exams.first;

public class PetClient {

	public static void main(String[] args) {
		
		Pet p1 = new Pet();
		System.out.println(p1.toString());
		
		Pet p2 = new Pet("Bobby", 2);
		System.out.println(p2.toString());
		
		System.out.println("Is the second pet the same as the first one? " + p2.equals(p1));

	}

}
