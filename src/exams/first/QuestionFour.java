/*
 * Write a program that prompts the user for 5 words and prints out any word that appears more than once.
 * Dongelei Lin
 */

package exams.first;

import java.util.Scanner;

public class QuestionFour {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		
		String[] words = new String[5];
		
		for (int i = 0; i < words.length; i++) {
			System.out.println("Enter a word or character: ");
			words[i] = input.nextLine();
		}
		
		input.close();
		// check which word is repeated 
		for (int i = 0; i < words.length; i++) {
			String w = words[i];
			
			int count = 0;
			
			for (int y = 0; y < words.length && i != y; y++) {
				String s = words[y];
				if (s.compareTo(w) == 0) {
					count ++;
				}
			}
			
			if (count > 0) {
				System.out.println("This word " + w + " is repeated " + count + " times.");
			}
			
		}
		
	}

}
