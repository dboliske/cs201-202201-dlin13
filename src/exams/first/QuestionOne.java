/* 
 * Write a program that prompts the user for an integer, add 65 to it, convert the result to a character and print that character to the console.
 * Donglei Lin
 */

package exams.first;

import java.util.Scanner;

public class QuestionOne {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		
		System.out.println("Enter any integer. It will return the character that's assign to this (integer + 65) ");
		
		System.out.print("Enter an integer here: ");
		int i = Integer.parseInt(input.nextLine());
		int r = +(65 + i);
		char out = (char) r;
		
		System.out.println(out);
		
		input.close();
		

	}

}
