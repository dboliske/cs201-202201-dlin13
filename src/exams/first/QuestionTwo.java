/*
 * Write a program that prompts the user for an integer. 
 * If the integer is divisible by 2 print out "foo", and 
 * if the integer is divisible by 3 print out "bar". 
 * If the integer is divisible by both, your program should print out "foobar" and 
 * if the integer is not divisible by either, then your program should not print out anything.
 * Donglei lin 
 */

package exams.first;

import java.util.Scanner;

public class QuestionTwo {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		
		System.out.print("Enter an integer: ");
		
		int n = Integer.parseInt(input.nextLine());
		
		if (n % 2 == 0 && n % 3 != 0 ) {
			System.out.println("foo");
		} else if (n % 3 == 0 && n % 2 != 0) {
			System.out.println("bar");
		} else if (n % 2 == 0 && n % 3 == 0) {
			System.out.println("foobar");
		}
		
		input.close();
	}
	
}

