package playground;

import java.util.Objects;

public class Classroom {
	protected String building;
	protected String roomNumber;
	private int seats;
	
	public Classroom() {
		building = "Main";
		roomNumber = "lab1";
		seats = 1;
	}
	
	public Classroom(String building, String roomNumber, int seats) {
		this.building = building;
		this.roomNumber = roomNumber;
		this.seats = Math.abs(seats);
	}

	public String getBuilding() {
		return building;
	}

	public void setBuilding(String building) {
		this.building = building;
	}

	public String getRoomNumber() {
		return roomNumber;
	}

	public void setRoomNumber(String roomNumber) {
		this.roomNumber = roomNumber;
	}

	public int getSeats() {
		return seats;
	}

	public void setSeats(int seats) {
		this.seats = seats;
	}
	
	@Override
	public String toString() {
		String info = "Seat # " + seats + " at room #" + roomNumber + " of " + building;
		return info;
	}


	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Classroom other = (Classroom) obj;
		return Objects.equals(building, other.building) && Objects.equals(roomNumber, other.roomNumber)
				&& seats == other.seats;
	}

	
}
