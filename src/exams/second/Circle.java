package playground;

public class Circle extends Polygon {
	private double radius;
	
	public Circle() {
		super();
		radius = 0.0;
	}

	public Circle(double radius) {
		this.radius = radius;
	}
	
	@Override
	public double area() {
	
		return Math.PI * this.getRadius() * this.getRadius();
	}

	@Override
	public double perimeter() {
		
		return 2.0 * Math.PI * this.getRadius();
	}

	public double getRadius() {
		return radius;
	}

	public void setRadius(double radius) {
		this.radius = radius;
	}

}
