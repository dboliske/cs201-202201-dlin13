/* Jump Search class
 * This Java program implements the Jump Search Algorithm recursively for the following Array (or ArrayList) of numbers 
 * and allows the user to search for a value, print the position of where that value is found. 
 * If the value is not present, it will print -1.
 * {0.577, 1.202, 1.282, 1.304, 1.414, 1.618, 1.732, 2.685, 2.718, 3.142}
 * Donglei Lin, session 2
 * 2022/4/29
 */

package exams.second;

import java.util.Scanner;

public class QuestionFive {

	public static int search(Double[] array, double value) {
		int step = (int)Math.sqrt(array.length);
		int prev = 0;
	
		while (Double.compare(array[Math.min(step, array.length - 1)],value) < 0) {
			prev = step;
			step += (int)Math.sqrt(array.length);
			if (prev >= array.length) {
				return -1;
			}
		}
		
		if (Double.compare(array[Math.min(step, array.length - 1)],value) == 0) {
			return step;
		}
		
		while (Double.compare(array[prev],value) < 0) {
			prev++;
			if (prev == Math.min(step, array.length - 1)) {
				return -1;
			}
		}
		
		if (Double.compare(array[prev], value) == 0) {
			return prev;
		}
		
		return -1;
	}

	public static void main(String[] args) {
		Double[] numbers = {0.577, 1.202, 1.282, 1.304, 1.414, 1.618, 1.732, 2.685, 2.718, 3.142};
		
		Scanner input = new Scanner(System.in);
		System.out.println("Enter a number to search. If the number you entered is not present in the array it will print '-1', else '1' ");
		System.out.print("Search term: ");
		double value = Double.parseDouble(input.nextLine());
		int index = search(numbers, value);
		if (index == -1) {
			System.out.println(-1);
			System.out.println("The value " + value + " is not found.");
		} else {
			System.out.println(1);
			System.out.println("The value " + value + " found at index " + index + ".");
		}

		input.close();
	}

}
