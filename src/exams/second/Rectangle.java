package playground;

public class Rectangle extends Polygon {
	private double width;
	private double height;
	
	
	public Rectangle() {
		super();
		width = 0.0;
		height = 0.0;	
	}
	
	public Rectangle(double width, double height) {
		this.width = width;
		this.height = height;
	}

	public double getWidth() {
		return width;
	}

	public void setWidth(double width) {
		this.width = width;
	}

	public double getHeight() {
		return height;
	}

	public void setHeight(double height) {
		this.height = height;
	}
	
	@Override
	public double area() {
		
		return this.height * this.width;
	}
	
	@Override
	public double perimeter() {
		
		return 2.0 * (this.getHeight() + this.getWidth());
	}
	
	@Override
	public String toString() {
		return "Rectangle [width=" + width + ", height=" + height + "]";
	}
}
