package playground;

import java.util.Objects;

public class ComputerLab extends Classroom {

	private boolean computers;
	
	public ComputerLab () {
		super();
		computers = true;
	}
	
	public ComputerLab (String building, String roomNumber, int seats, boolean computers) {
		super (building, roomNumber, seats);
		this.computers = computers;
	}
	
	public boolean getComputers() {
		return computers;
	}
	
	public void setComputers() {
		this.computers = computers;
	}
	
	public boolean hasComputers() {
		return computers;
	}

	@Override
	public String toString() {
		if (!hasComputers()) {
			return super.toString() + " has no computers available. ";	
		} else {
			return super.toString() + " has computers available. ";
		}
		
	}


	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		ComputerLab other = (ComputerLab) obj;
		return computers == other.computers;
	}
	


}
