# Final Exam

## Total

95/100

## Break Down

1. Inheritance/Polymorphism:    19/20
    - Superclass:               5/5
    - Subclass:                 5/5
    - Variables:                5/5
    - Methods:                  4/5
2. Abstract Classes:            17/20
    - Superclass:               5/5
    - Subclasses:               5/5
    - Variables:                4/5
    - Methods:                  3/5
3. ArrayLists:                  19/20
    - Compiles:                 5/5
    - ArrayList:                5/5
    - Exits:                    5/5
    - Results:                  4/5
4. Sorting Algorithms:          20/20
    - Compiles:                 5/5
    - Selection Sort:           10/10
    - Results:                  5/5
5. Searching Algorithms:        20/20
    - Compiles:                 5/5
    - Jump Search:              10/10
    - Results:                  5/5

## Comments

1. Allows zero seats (non-postive)
2. "The radius, height, and width should always be positive and can default to 1." Polygon var should be protected, not private.
3. Error on non-number input
4.
5.
