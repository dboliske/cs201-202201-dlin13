/* Selection Sort class
 * This Java program implements the Selection Sort Algorithm for the below Array (or ArrayList) of Strings and prints the sorted results.
 * {"speaker", "poem", "passenger", "tale", "reflection", "leader", "quality", "percentage", "height", "wealth", "resource", "lake", "importance"}
 * Donglei Lin, session 2
 * 2022/4/29
 */

package exams.second;

public class QuestionFour {

	public static String[] sort(String[] array) {
		for (int i=0; i<array.length - 1; i++) {
			int min = i;
			for (int j=i+1; j<array.length; j++) {
				if (array[j].compareTo(array[min]) < 0) {
					min = j;
				}
			}
			
			if (min != i) {
				String temp = array[i];
				array[i] = array[min];
				array[min] = temp;
			}
		}
		
		return array;
	}

	public static void main(String[] args) {
		String[] lang = {"speaker", "poem", "passenger", "tale", "reflection", "leader", "quality", "percentage", "height", "wealth", "resource", "lake", "importance"};
		
		lang = sort(lang);
		
		for (String l : lang) {
			System.out.print(l + " ");
		}
	}


}
