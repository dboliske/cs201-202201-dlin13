package playground;

abstract class Polygon {
	
	private String name;
	
	public Polygon() {
		name = null;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	abstract public double area();
	
	abstract public double perimeter();

	
	@Override
	public String toString() {
		return "Polygon [name=" + name + "]";
	}

	

}
