/* ArrayList class
 * This Java program prompts the user to enter a sequence of numbers, storing them in an ArrayList, and continues prompting the user for numbers until they enter "Done". 
 * When they have finished entering numbers, your program should return the minimum and maximum values entered.
 * Donglei Lin, session 2
 * 2022/4/29
 */

package exams.second;

import java.util.ArrayList;
import java.util.Scanner;


public class QuestionThree {
	
	public static ArrayList<Double> number (Scanner input) {
		
		// create user input list
		ArrayList<Double> numbers = new ArrayList<Double>();
		boolean done = false;
		System.out.println("Enter a number, can be an integer or with decimal. ");
		System.out.println("\n Type 'done' when finished. ");
		
		do {
			String s = getUserInput(input);
			if (!s.equalsIgnoreCase("done")) {
				double num = Double.parseDouble(s);
				numbers.add(num);	
			} else {
				done = true;
			}
			
		}
		while (!done);
		
		return numbers;
	}
	
	public static ArrayList<Double> sort(ArrayList<Double> array) {
		for (int i=0; i<array.size() - 1; i++) {
			// find the index of the largest double in the subarray
			int max = indexOfLargestDouble(array,array.size()-i);
			
			// swap array[max] and array[array.length-i-1]
			Double temp = array.get(max);
			array.set(max, array.get(array.size() - 1 - i));
			array.set((array.size()-i-1),temp);
		}
		
		return array;
	}

	public static int indexOfLargestDouble(ArrayList<Double> array, int size) {
		
		// find the index of the largest number in the array
		int index = 0;
		for (int i=1; i<size; i++) {
			System.out.println(Double.compare(array.get(i), array.get(index)));
			if (Double.compare(array.get(i), array.get(index)) ==1)  {
				index = i;
				System.out.println("new index: " + index);
			}
		} 
		return index;
	}
	
	private static String getUserInput(Scanner input) {
        String choice = input.nextLine();
        return choice;
    }
	
	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		// collect user input
		ArrayList<Double> userInput = number(input);
		// sort user input array in the order of min - > max
		ArrayList<Double> sorted = sort(userInput);
		// print out the min and max number of the sorted array
		System.out.println("the min of these numbers is : " + sorted.get(0));
		System.out.println("the max of these numbers is : " + sorted.get(sorted.size()-1));

	}

}
