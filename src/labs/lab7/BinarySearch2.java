/*
 * Alternative way to doing Binary Search
 * Write a Java program that will implement the Binary Search algorithm as a recursive method
 * and be able to search the following array of Strings for a specific value, input by the user:
 * {"c", "html", "java", "python", "ruby", "scala"}
 * Donglei Lin
 */

package labs.lab7;

import java.util.Scanner;

public class BinarySearch2 {

	public static int search(String[] array, String value) {
		int start = 0;
		int end = array.length-1;
		int pos = -1;
		boolean found = false;
		while (!found && end >= start) {
			int middle = (start + end) / 2;
			if (array[middle].equalsIgnoreCase(value)) {
				found = true;
				pos = middle;
			} else if (array[middle].compareToIgnoreCase(value) > 0) {
				end = middle - 1;
			} else {
				start = middle + 1;
			} 
		}
		
		return pos;
	}

	public static void main(String[] args) {
		String[] lang = {"c", "html", "java", "python", "ruby", "scala"};
		
		Scanner input = new Scanner(System.in);
		System.out.print("Search term: ");
		String value = input.nextLine();
		int index = search(lang, value);
		if (index == -1) {
			System.out.println(value + " not found.");
		} else {
			System.out.println(value + " found at index " + index + ".");
		}

		input.close();
	}
	
}
