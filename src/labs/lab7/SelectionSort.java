/*
 * Alternative way of doing Selection Sort:
 * Java program that will implement the Selection Sort algorithm as a method and sorts the following array of doubles:
 * {3.142, 2.718, 1.414, 1.732, 1.202, 1.618, 0.577, 1.304, 2.685, 1.282}
 * Donglei Lin
 */

package labs.lab7;

public class SelectionSort {
	
	public static Double[] sort(Double[] array) {
		for (int i=0; i<array.length - 1; i++) {
			// find the index of the largest double in the subarray
			int max = indexOfLargestDouble(array,array.length-i);
			
			// swap array[max] and array[array.length-i-1]
			Double temp = array[max];
			array[max] = array[array.length-i-1];
			array[array.length-i-1] = temp;
		}
		
		return array;
	}

	public static int indexOfLargestDouble(Double[] array, int size) {
		int index = 0;
		for (int i=1; i<size; i++) {
			if (array[i] > array[index]) {
				index = i;
			}
		} 
		return index;
	}
	
	
	public static void main(String[] args) {
		Double[] doubles = {3.142, 2.718, 1.414, 1.732, 1.202, 1.618, 0.577, 1.304, 2.685, 1.282};
		
		doubles = sort(doubles);
		
		for (Double l : doubles) {
			System.out.print(l + " ");
		}

	}

}
