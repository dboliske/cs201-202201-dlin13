/*
 * Java program that will implement the Selection Sort algorithm as a method and sorts the following array of doubles:
 * {3.142, 2.718, 1.414, 1.732, 1.202, 1.618, 0.577, 1.304, 2.685, 1.282}
 * Donglei Lin
 */

package labs.lab7;

public class SelectionSort2 {
	
	public static Double[] sort(Double[] array) {
		for (int i=0; i<array.length - 1; i++) {
			int min = i;
			for (int j=i+1; j<array.length; j++) {
				if (array[j] < array[min]) {
					min = j;
				}
			}
			
			if (min != i) {
				Double temp = array[i];
				array[i] = array[min];
				array[min] = temp;
			}
		}
		
		return array;
	}

	public static void main(String[] args) {
		Double[] doubles = {3.142, 2.718, 1.414, 1.732, 1.202, 1.618, 0.577, 1.304, 2.685, 1.282};
		
		doubles = sort(doubles);
		
		for (Double l : doubles) {
			System.out.print(l + " ");
		}

	}

}
