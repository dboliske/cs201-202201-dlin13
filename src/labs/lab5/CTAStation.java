/* 
 * CTAStations class
 * This application inherit from GeoLocation. And following the UML diagram in CTA-Application.png
 * Donglei Lin   section 2
 */

package labs.lab5;

public class CTAStation extends GeoLocation {

	private String name;
	private String location;
	private boolean wheelchair;
	private boolean open;

	public CTAStation() {
		super();

	}

	public CTAStation(String name, double lat, double lng, String location, boolean wheelchair, boolean open) {
		super(lat, lng);
		this.name = name;
		this.location = location;
		this.wheelchair = wheelchair;
		this.open = open;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public boolean hasWheelchair() {
		return wheelchair;
	}

	public void setWheelchair(boolean wheelchair) {
		this.wheelchair = wheelchair;
	}

	public boolean isOpen() {
		return open;
	}

	public void setOpen(boolean open) {
		this.open = open;
	}
	
	@Override
	public String toString() {
		return this.name + "location is: " + super.toString();
	}
	
	@Override
	public boolean equals(Object obj) {
		if (!super.equals(obj)) {
			return false;
		} else if (!(obj instanceof CTAStation)) {
			return false;
		}
		
		CTAStation st = (CTAStation)obj;
		if (this.name != st.getName() || this.location != st.getLocation() || this.wheelchair != st.hasWheelchair() || this.open != st.isOpen()) {
			return false;
		}
		
		return true;
	}
	

}
	
