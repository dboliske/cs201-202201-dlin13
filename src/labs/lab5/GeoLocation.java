/* 
 * GeoLocation class
 * This class is constructed following the UML Diagram of GeoLocation
 * Donglei Lin， section 2
 */

package labs.lab5;

public class GeoLocation {

	private double lat;
	private double lng;
	
	public GeoLocation() {
		lat = 0.0;
		lng = 0.0;
	}
	
	public GeoLocation(double latitude, double lngitude) {
		lat = latitude;
		lng = lngitude; 
	}
	
	public void setLat(double lat) {
		this.lat = lat;
	}
	 public void setLng(double lng) {
		 this.lng = lng;
	 }
	 
	 public double getLat() {
		 return lat;
	 }

	 public double getLng() {
		 return lng;
	 }
	 
	 public String toString() {
		 return "( " + lat + ", " + lng + ") ";

	 }
	 
	 public boolean validLat(GeoLocation g) {
		 return (g.getLat() >= -90 && g.getLat() <= +90);
		 }
	 
	 public boolean validLng(GeoLocation g) {
		 return (g.getLng() >= -180 && g.getLng() <= +180);
		 }

	 public double calcDistance(double lati, double lngi) {
		 double d = Math.sqrt(Math.pow(lati - this.lat, 2)) + Math.sqrt(Math.pow(lngi - this.lng, 2));
		 return d;
	 }
}
