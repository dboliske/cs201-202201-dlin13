/* 
 * CTAStopApp class
 * This application class will display a menu of options that the user can select from to display stations that meet certain criteria.
 * Donglei Lin   section 2
 */

package labs.lab5;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;


public class CTAStopApp {

	public static CTAStation[] readFile(String filename) {
		CTAStation[] stations = new CTAStation[2];
		int count = 0;
		System.out.println("Read file: " + filename);
		try {
			File f = new File(filename);
			Scanner input = new Scanner(f);
			
			// load CTA stops from file
			while (input.hasNextLine()) {
				try {
					String line = input.nextLine();
					String[] values = line.split(",");
					CTAStation s = null;
					s = new CTAStation(values[0],
		                      Double.parseDouble(values[1]),
		                      Double.parseDouble(values[2]),
		                      values[3],
		                      Boolean.parseBoolean(values[4]),
		                      Boolean.parseBoolean(values[5])
		                      );
					if (stations.length == count) {
						stations = resize(stations, stations.length*2);
					}
					
					stations[count] = s;
					count++;					
				} catch (Exception e) {
					
				}
			}
			
			input.close();
			
		} catch (FileNotFoundException fnf) {
			System.out.println(fnf);
		} catch (Exception e) {
			System.out.println("Error occured reading in file.");
		}
		stations = resize(stations, count);
		return stations;
	}
	
	public static CTAStation[] resize(CTAStation[] data, int size) {
		CTAStation[] temp = new CTAStation[size];
		int limit = data.length > size ? size : data.length;
		for (int i=0; i<limit; i++) {
			temp[i] = data[i];
		}
		
		return temp;
	}
	
	
	public static void menu(Scanner input, CTAStation[] data) {
		boolean done = false;
		
		do {
			System.out.println("1. Display Station Names");
			System.out.println("2. Display Stations with/without Wheelchair access");
			System.out.println("3. Display Nearest Station");
			System.out.println("4. Exit");
			System.out.print("Choice: ");
			String choice = input.nextLine();
			
			switch (choice) {
				case "1": // Display Station Names
					displayStationNames(data);
					break;
				case "2": // Display Stations with/without Wheelchair access
					displayByWheelchair(data, input);
					break;
				case "3": // Show the nearest station
					displayNearest(data, input);
					break;
				case "4": // Exit
					done = true;
					break;
				default:
					System.out.println("That is not a valid choice, please try again.");
			}
		} while (!done);

	}
	

	public static void displayStationNames(CTAStation[] data) {
		for (int i=0; i<data.length; i++) {
			System.out.println(data[i].getName());
		}

	}
	
	public static void displayByWheelchair(CTAStation[] data, Scanner input) {
		boolean acc = yesNoPrompt("Display stations with wheelchair access?", input);
		if (acc) {
			for (int i=0; i<data.length; i++) {
				if (data[i].hasWheelchair()) {
					System.out.println(data[i].getName());
				} 
			}
		} else if (!acc) {
			System.out.println("Showing all CTAStaions: \n");
			displayStationNames(data);
		}
		
	}
	
	public static void displayNearest(CTAStation[] data, Scanner input) {
		System.out.print("Your Latitude: ");
		Double lati = input.nextDouble();
		System.out.print("Your Longitude: ");
		Double lngi = input.nextDouble();
		
		double d = data[0].calcDistance(lati, lngi);
		String nearest = data[0].getName();
		for (int i=1; i<data.length; i++) {
			if (data[i].calcDistance(lati, lngi) < d) {
				d = data[i].calcDistance(lati, lngi);
				nearest = data[i].getName();
			}
		}
		System.out.println("The nearest station is: " + nearest);			
	}
	
	public static boolean yesNoPrompt(String prompt, Scanner input) {
		System.out.print(prompt + " (y/n): ");
		boolean result = false;
		String yn = input.nextLine();
		switch (yn.toLowerCase()) {
			case "y":
			case "yes":
				result = true;
				break;
			case "n":
			case "no":
				result = false;
				break;
			default:
				System.out.println("That is not yes or no. Please try again.");
		}
		
		return result;
	}
	
	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		System.out.print("Load file: ");
		String filename = input.nextLine();
		// Load file (if exists)
		CTAStation[] data = readFile(filename);
		menu(input, data);
		input.close();
		System.out.println("Goodbye!");
	}
}
