package labs.lab0;

import java.util.Scanner;

public class MyInfo {

	public static void main(String[] args) {
		// print out my name and birthday in the following format: 
		// My name is Mickey Mouse and my birthdate is Aug. 20, 1959.
		String myname = "Donglei Lin";
		String dob = "Dec. 22,  1983";
		
		System.out.println("My name is " + myname +  " " + "my birthday is " + dob + ".");
		
	}

}
