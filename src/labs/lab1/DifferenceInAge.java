/* DifferenceInAge class
 * Donglei lin
 */

package labs.lab1;

import java.util.Scanner;

public class DifferenceInAge {

	public static void main(String[] args) {
		// create a scanner for reading in user input
		Scanner input = new Scanner(System.in);
		
		// prompt user to enter his/her age
		System.out.print("Your age: ");
		double x = Double.parseDouble(input.nextLine());
		
		// prompt user to enter his/her father's age
		System.out.print("And your father's age: ");
		double y = Double.parseDouble(input.nextLine());
		
		// calculate the difference in age as the user's father's age - the user's age
		System.out.println("You're are " + +(y-x) + " years younger than your father.");
		
		input.close();
	}

}
