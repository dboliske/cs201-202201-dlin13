/* Temperature class
 * Donglei Lin
 */

/* 
 * Java program that asks user for temperature in Fahrenheit, then convert and display in Celsius
 * Formula: (Fahrenheit - 32) * 5/9 = Celsius
 */


package labs.lab1;

import java.util.Scanner;

public class CelsiusToFahrenheit {

	public static void main(String[] args) {
		// create a scanner to read in user input
		Scanner input = new Scanner(System.in);
		
		// prompt user to enter a temperature in Fahrenheit
		System.out.print("Temperature in Fahrenheit: ");
		double f = Double.parseDouble(input.nextLine());
		
		// convert it to Celsius
		System.out.println("It's " + +((f - 32) * 5/9) + " in Celsius.");
		
		input.close();
		

	}

}
