/* Echo user' name class
 * Donglei Lin
 */

package labs.lab1;

import java.util.Scanner;

public class Initial {

	public static void main(String[] args) {
		// create a variable for reading in user input
		Scanner input = new Scanner(System.in);
		
		// prompt the user to enter his/her name
		System.out.print("Enter your name here: ");
		
		//read in the next line of the text entered by the user
		String name = input.nextLine();
		
		// echo the name entered by the user
		System.out.println("Echo: " + name);
				
		input.close();
	}

}
