/* Initial class
 * Donglei Lin
 */

/* 
 * Ask the user for their first name then print out the initial letter
 */

package labs.lab1;

import java.util.Scanner;

public class EchoUserName {

	public static void main(String[] args) {
		// create a variable for reading in user input
		Scanner input = new Scanner(System.in);
		
		// prompt the user for their first name
		System.out.print("Enter your first name here: ");
		
		//read in the first letter (initial) letter
		char i = input.nextLine().charAt(0);
		
		// print out the initial letter of user's first name
		System.out.println("Intial: " + i);
				
		input.close();
	}

}
