/* InchToCentimeter class
 * Donglei Lin
 */

/*
 * Java program to convert length in inches entered from the user to centimeters. The conversion formula is:
 * Inch * 2.54 = Centimeter
 */

package labs.lab1;

import java.util.Scanner;

public class InchToCentimeter {

	public static void main(String[] args) {
		// create a scanner to read in user input
		Scanner input = new Scanner(System.in);
		
		// prompt the user to enter any inches
		System.out.print("Enter any length in inches: ");
		double i = Double.parseDouble(input.nextLine());
		
		// convert inches to centimeters
		System.out.println(i + " is equal to " + +(i * 2.54) + " centimeters."); // multiply the length value by 2.54
		
		input.close();

	}

}

/* Test table 
   This program has been tested very various numbers generated from excel's =RANDARRAY(10,1,1,20,). Test results matches
   targeted values. Below is a subset of the test results:
    Inch	Cm	    TestResult
	17.55	44.56	44.56
	8.26	20.98	20.98
	19.90	50.54	50.54
	4.99	12.67	12.67
	19.00	48.25	48.25
	13.21	33.57	33.57
	1.13	2.87	2.87
	8.18	20.79	20.79
	1.29	3.29	3.29
	11.69	29.68	29.68
*/