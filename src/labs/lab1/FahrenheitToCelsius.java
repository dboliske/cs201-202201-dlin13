/* FahrenheitToCelsius class
 * Donglei Lin
 */

/* 
 * Java program that converts Fahrenheit to Celsius or Celsius to Fahrenheit
 * Formula: 
 * 	(Celsius * 9/5) + 32 = Fahrenheit
 * 	(Fahrenheit - 32) * 5/9 = Celsius
 */	


package labs.lab1;

import java.util.Scanner;

public class FahrenheitToCelsius {

	public static void main(String[] args) {
		// create a scanner to read in user input
		Scanner input = new Scanner(System.in);
		
		// prompt user to enter the temperature to be converted
		System.out.print("Choose which unit is to be converted: F / C ? ");
		char u = input.nextLine().charAt(0);
		
		// ask the user for temperature
		System.out.print("Enter any degree: ");
		double d = Double.parseDouble(input.nextLine());
		
		// pick options from user input
		if (u == 'F') {
			System.out.println("It's " + +((d - 32) * 5/9 )+ " in Celsius.");
		} else if (u == 'C') {
			System.out.println("It's " + +((d * 9/5) + 32) + " in Fahrenheit.");
		}
		
		input.close();
		

	}

}

/*
 * This Java program has been tested on sets of random numbers generated using excel's =RANDARRAY(10,1,-30,200,FALSE), from F to C and C to F.
 * Below is a subset of test results from the test table, which matches the expected number. 					
	Targeted   Targeted Test F to C Test C to F
	Fahrenheit Celsius	Java Output	Java Output
	86.25		30.14	30.14	30.14
	122.08		50.05	50.05	50.05
	156.71		69.28	69.28	69.28
	16.84		-8.42	-8.42	-8.42
	131.17		55.10	55.10	55.10
	62.96		17.20	17.20	17.20
	18.22		-7.66	-7.66	-7.66
	-21.31		-29.62	-29.62	-29.62
	31.92		-0.04	-0.04	-0.04
	162.45		72.47	72.47	72.47
*/
