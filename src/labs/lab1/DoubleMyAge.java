/* DoubleMyAge class
 * Donglei Lin
 */

/* 
   program to perform simple multiplication 
   prompt the user to enter his/her age, print out age*2
 */

package labs.lab1;

import java.util.Scanner;

public class DoubleMyAge {

	public static void main(String[] args) {
		// create a scanner for reading in user input
		Scanner input = new Scanner(System.in);
		
		// prompt user to enter his/her age
		System.out.print("Your age: ");
		double x = Double.parseDouble(input.nextLine());
		
		//multiply user's age by 2
		System.out.println("Your age times 2 equals to : " + +(x*2));
		
		input.close();

	}

}
