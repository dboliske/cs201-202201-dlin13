/* InchToFeet class
 * Donglei Lin
 */

/* 
 * This program asks the user for his/her height in inches without decimals, and convert it to feet and inch convention
 * conversion is done by using both mode and division operators on user input:
 * feet         = inch / 12;
 * leftOverInch = inch % 12;
 */

package labs.lab1;

import java.util.Scanner;

public class InchToFeet {

	public static void main(String[] args) {
		// create a scanner for reading in user input
		Scanner input = new Scanner(System.in);
		
		// prompt user to enter his/her age in inches
		System.out.print("Please enter your height in inches, round to the nearest inch, no decimal: ");
		int x = input.nextInt();
		
		// convert it to feet and inch convention
		System.out.println("You're " + +(x / 12) + " foot and " + +(x % 12) + " inch tall");
		
		input.close();
		

	}
}
