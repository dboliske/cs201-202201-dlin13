/* InchToCm class
 * Donglei Lin
 */

/* 
 * This program asks the user for his/her height in unit of inch then convert it to unit of centimeter
 * conversion is done by multiplying inch value by 2.54
 */

package labs.lab1;

import java.util.Scanner;

public class InchToCm {

	public static void main(String[] args) {
		// create a scanner for reading in user input
		Scanner input = new Scanner(System.in);
		
		// prompt user to enter his/her age
		System.out.print("Your height in inches: ");
		double x = Double.parseDouble(input.nextLine());
		
		// multiply inch by 2.54
		System.out.println("Your height is equal to : " + +(x*2.54) + " in centimeters.");
		
		input.close();
		

	}
}
