/* VolumeCalculator class
 * Donglei Lin
 */

/*
 * Java program that calculates the square feet of woods needed to make a box.
 * It asks the user to enter any length, width and depth in inches. Total square feet is measured by the below formula:
 * Total square feet = (width * height) * 2 + (length * height) * 4
 */


package labs.lab1;

import java.util.Scanner;

public class VolumeCalculator {

	public static void main(String[] args) {
		// create a scanner to read in user inputs
		Scanner input = new Scanner(System.in);
		
		// prompt the user for length 
		System.out.print("Enter the length of the box: ");
		double l = Double.parseDouble(input.nextLine());
		
		// prompt the user for width
		System.out.print("Enter the width of the box: ");
		double w = Double.parseDouble(input.nextLine());
		
		// prompt the user for height
		System.out.print("Enter the height of the box: ");
		double h = Double.parseDouble(input.nextLine());
		
		// calculate and print out total square feet needed
		double sf = (l * h) * 4 + (w * h) * 2;
		System.out.println("Total square feet is calculated as : (width * height) * 2 + (length * height) * 4 \n"
						   + "The total is : " + sf);
		
		input.close();

	}

}


/* 
   Test this program with various random numbers generated by excel's =RANDARRAY(). Test results equal to target output.
   Below is an abstract of the test table:
    Length  Width   Height  Sqrt.F  TestResult
	9.19	4.37	8.68	394.94	394.94
	6.71	5.33	8.12	304.50	304.50
	14.33	7.75	8.25	600.77	600.77
	9.09	2.14	7.60	308.86	308.86
	17.33	7.25	13.00	1089.66	1089.66
	18.07	1.34	9.20	689.63	689.63
	18.45	3.59	7.51	608.16	608.16
	12.03	7.55	9.25	584.79	584.79
	9.74	7.15	11.29	601.31	601.31
	16.61	2.10	5.72	404.06	404.06
*/