/*
 *  CollectAndWrite class
 *  This program will continue to prompt the user for numbers, storing them in an array till they're finished, then prompt the
 *  user for a file name so that the values can be saved to that file.
 *  Donglei Lin
 *  CS 201 Session 2
 */

package labs.lab3;

import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;

public class CollectAndWrite {

	public static void main(String[] args) {
		int count = 0; // initialize the starting index to write input
		int[] userNums = new int[3]; // initialize an array to hold numbers
		
		Scanner input  = new Scanner(System.in); // read in user input
		
		boolean done = false;
		
		do {
			System.out.println("a. Enter a number");
			System.out.println("b. Done");
			
			System.out.print("Choice: ");
			String choice = input.nextLine();
			switch(choice) {
				case "a": 
					System.out.println("number: ");
					// resize array when user input is larger than array size
					if (count == userNums.length) {
						int[] bigger = new int[2 * userNums.length];
						for (int y=0; y<userNums.length; y++) {
							bigger[y] = userNums[y];
						}
						userNums = bigger;
						bigger = null;
					}
					
					userNums[count] = Integer.parseInt(input.nextLine());
					count++;
					break;
				case "b":
					System.out.println("file name: ");
					String filename = input.nextLine();
					System.out.println("Saving file to: 'src/labs/lab3/'" + filename + ".txt");
					try {
						FileWriter f = new FileWriter("src/labs/lab3/" + filename + ".txt");
						
						for (int i=0; i<userNums.length; i++) {
							f.write(userNums[i]+ "\n");	
						}
						f.flush();
						f.close();
					} catch (IOException e) {
						System.out.println(e.getMessage());
						System.out.println("File Not Saved!!!");
					}
					done = true;
					break;
				default:
					System.out.println("Not a valide choice, choose again. ");
			
			}
		} while (!done);
		
		input.close();
		System.out.println("Finished Writing.");

	}

}
