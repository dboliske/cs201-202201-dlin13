/*
 *  FindMinimum class
 *  This program will find the minimum value of an array and print it to the console
 *  Donglei Lin
 *  CS 201 Session 2
 */

package labs.lab3;

public class FindMinimum {

	public static void main(String[] args) {
		int[] arr = {72, 101, 108, 108, 111, 32, 101, 118, 101, 114, 121, 111, 110, 101, 33, 32, 76, 111, 111, 107, 32, 97, 116, 32, 116, 104, 101, 115, 101, 32, 99, 111, 111, 108, 32, 115, 121, 109, 98, 111, 108, 115, 58, 32, 63264, 32, 945, 32, 8747, 32, 8899, 32, 62421};

		int min = arr[0]; // initialize a pseudo minimum value
		for (int i=0; i<arr.length; i++) {
			if (arr[i] < min) {
				min = arr[i]; 
			}
		}
		
		System.out.println("The minimum of this array: "); 
		System.out.println("is: " + min );
	
	}

}
