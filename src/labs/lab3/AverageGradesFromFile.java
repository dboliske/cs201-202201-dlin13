/* 
 * AverageGradesFromFile class
 * compute the average grades of a class and print it out to the console
 * Donglei Lin
 * CS 201 Session 2
 */



package labs.lab3;

import java.io.File;
import java.io.IOException;
import java.util.Scanner;

public class AverageGradesFromFile {

	public static void main(String[] args) throws IOException {
		// read the grades in from a file
		File f = new File("src/labs/lab3/grades.csv");
		Scanner input = new Scanner(f);
		
		// loop through the file to get the row count
		int row = 0;
		while (input.hasNextLine()) {
			row++;
			System.out.println("This is the: " + row + " row for the file: " + input.nextLine());
		}
		
				
		// create name and grade arrays
		int[] grade = new int[row];
		String[] name = new String[row];
		
		// reinstate input, loop the file a 2nd time
		input = new Scanner(f);	
		while (input.hasNextLine()) {
			for (int i=0; i<grade.length; i++) {
				String line = input.nextLine();
				String[] values = line.split(",");
				name[i] = values[0];
				grade[i] = Integer.parseInt(values[1]);
			}
		}
		
		input.close();
		
		// compute average and print it out
		int total = 0;
		for (int i=0; i<grade.length; i++) {
			total = total + grade[i];
		}
		System.out.println("Total student: " + grade.length + " Total score is: " + total);
		System.out.println("Average: " + (total / grade.length));
	}

}
