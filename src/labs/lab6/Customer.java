/* 
 *  Customer class
 *  Donglei Lin
 */

package labs.lab6;

public class Customer {
	
	private String name;
	
	public Customer() {
		this.name = "John";
	}
	
	public Customer(String name) {
		this.name = name;
	}
	 
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public String toString() {
		return name;
	}

}
