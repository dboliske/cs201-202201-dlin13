/* 
 *  Java application that handles a queue at a deli counter. 
 *  It allows customers to join the queue and deli workers to remove customers from the queue.
 *  Donglei Lin
 */

package labs.lab6;

import java.util.ArrayList;
import java.util.Scanner;


public class CustomerQueue {

	public static ArrayList<Customer> openDeliCounter(ArrayList<Customer> queue, Scanner input) {
		
		System.out.print("Enter Customer name: ");
		String name = input.nextLine();
		queue.add(new Customer(name));
		
		return queue;
		
	}

	
	public static void showQueue(ArrayList<Customer> workOrder) {
		int i = 1;
		for (Customer a : workOrder) {
			System.out.println("Order " + i + " : Customer name : " + a.toString());
			i++;
		}
	}
	
	public static ArrayList<Customer> nextCustomer(ArrayList<Customer> workOrder, Scanner input) {
		
		while (workOrder.size() !=0) {
			boolean o = nextOrder("Take order? ", input); // Decide whether to work on the next order or not	
			if (o) {
				input = new Scanner(System.in);
				boolean next = nextOrder("Help the next customer in line? ", input); // choose which customer to be served first rather than taking them on a first-come-first-serve sequence
				
				if (next) {
					System.out.println("Now serving next customer: " + workOrder.get(0));
					workOrder.remove(0);
				} else if (!next) { 
					System.out.println("Enter order number: ");
					int p = input.nextInt();
					System.out.println("Now serving customer: " + workOrder.get(p));
					workOrder.remove(p);	
				} else {
					break;
				}						
			}  
		}
		
		if (workOrder.size() ==0) {
			System.out.println("All customers have been helped. ");
		}
						
		return workOrder;
	}
	
	
	public static boolean nextOrder(String prompt, Scanner input) {
		System.out.print(prompt + " (y/n): ");
		boolean result = false;
		String yn = input.nextLine();
		switch (yn.toLowerCase()) {
			case "y":
			case "yes":
				result = true;
				break;
			case "n":
			case "no":
				result = false;
				break;
			default:
				System.out.println("That is not yes or no. Using no.");
		}
		
		return result;
	}
	
	public static ArrayList<Customer> menu(Scanner input, ArrayList<Customer> workOrder) {
		boolean done = false;
		
		String[] options = {"Open Deli Center", "Serve Next Customer", "List Customers", "Exit"};
		
		do {
			for (int i=0; i<options.length; i++) {
				System.out.println((i+1) + ". " + options[i]);
			}
			System.out.print("Choice: ");
			String choice = input.nextLine();
			
			switch (choice) {
				case "1": // open Deli counter to customers
					workOrder = openDeliCounter(workOrder, input);
					break;
				case "2": // help the next customer in line and remove from wait line
					workOrder = nextCustomer(workOrder, input);
					break;
				case "3": // List customers in line
					showQueue(workOrder);
					break;
				case "4": // Exit
					done = true;
					break;
				default:
					System.out.println("I'm sorry, but I didn't understand that.");
			}
		} while (!done);
		
		return workOrder;
	}
	
	
	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		
		ArrayList<Customer> workOrder = new ArrayList<Customer>();
		// Menu
		workOrder = menu(input, workOrder);
		
	}

}
