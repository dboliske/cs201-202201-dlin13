/*
 * MultipleChoice class
 * Donglei Lin
 */

/*
 * Java program that will repeatedly display a menu of choices to a user and prompt them to enter an option. 
 */

package labs.lab2;

import java.util.Scanner;

public class MultipleChoice {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		// print user menu
		System.out.println("Choose an option from below to perform: ");
		System.out.println("1. Say 'Hello'");
		System.out.println("2. Addition");
		System.out.println("3. Multiplication");
		System.out.println("4. Exit.");
		System.out.print("Choice: "); // get user's choice
		String choice = input.nextLine();
			
		boolean done = choice.equals("4"); // flag control variable
		while (!done) {
			switch (choice) {
				case "1":
					System.out.println("Hello");
					System.out.println();
					break;
				case "2":
					System.out.println("Perform addtion on 2 numbers. "
							+ "Enter the first number: ");
					double first = Double.parseDouble(input.nextLine());
					System.out.println("Enter the second number: ");
					double second = Double.parseDouble(input.nextLine());
					System.out.println("Total is : " + +(first + second));
					System.out.println();
					break;
				case "3":
					System.out.println("Perform muptiplicatoin on 2 numbers. "
							+ "Enter the first number: ");
					first = Double.parseDouble(input.nextLine());
					System.out.println("Enter the second number: ");
					second = Double.parseDouble(input.nextLine());
					System.out.println("Total is : " + +(first * second));
					System.out.println();
					break;
				case "4":
					done = true;
					break;
				default:
					System.out.println("That is not a valid choice.");
			}
			
			if (!done) {
				System.out.println("1. Say 'Hello'");
				System.out.println("2. Addition");
				System.out.println("3. Multiplication");
				System.out.println("4. Exit.");
				System.out.print("Choice: ");
				choice = input.nextLine(); // get user's choice
			}
		}
	
		input.close();
		
		System.out.println("Done.");

	}

}
