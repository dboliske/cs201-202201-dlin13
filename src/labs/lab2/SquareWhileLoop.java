/* 
 * SquareWhileLoop class
 * Donglei Lin
 */

/* 
 * Java program to print square pattern, using user defined size and character
 */

package labs.lab2;

import java.util.Scanner;

public class SquareWhileLoop {

	public static void main(String[] args) {
		// create a scanner to read in size
		Scanner input = new Scanner(System.in);
		
		// prompt the user
		System.out.print("Enter any size of the Square: ");
		int size = Integer.parseInt(input.nextLine());
		
		// create a scanner for character of the Square
		System.out.print("Enter any character of the Square: ");
		char cr  = input.nextLine().charAt(0);
		
		input.close();		
		
		int i, j;
		i = 0;
		while(i <= size) {
			j = 0;
			while(j <= size) {
				System.out.print(cr);
				j++;
			}
			i++;
			System.out.print("\n");
		}
		
		input.close();
		
		System.out.print("print square completed");

	}

}
