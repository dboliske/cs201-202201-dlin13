package labs.lab2;
// Java program to print Square pattern. This program allows user to choose the size of the square, a well the char(s) to print the Square. 
// Step 1: prompt user to enter the size of the Square;
// Step 2: prompt user to choose the char of the Square;
// Step 3: using for loop to create the Square;
// The class will create the Square like the one below:
// 
// ********
// ********
// ********
// ********
// ********
// ********
// ********
// ********

import java.util.Scanner;

public class SquareForLoop {

	public static void main(String[] args) {
		// create a scanner for size of the Square
		Scanner input = new Scanner(System.in);
		
		// prompt the user
		System.out.print("Enter any size of the Square: ");
		double s = Double.parseDouble(input.nextLine());
		
		// create a scanner for character of the Square
		System.out.print("Enter any character of the Square: ");
		char cr  = input.nextLine().charAt(0);
		
		input.close();
		
		int i, j;
		for (i = 1; i <= s; i++)
		{
			for(j = 1; j <=s; j++)
			{
				System.out.print(cr + "");
			}
			System.out.print("\n");
		}
				

	}

}
