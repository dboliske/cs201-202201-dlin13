/* 
 * AverageGrade class
 * Donglei Lin
 */

/* 
 * Java program that will prompt the user for the grades for an exam, computes the average, and returns the results.
 * The program is able to handle and unspecified number of grades.
 */

package labs.lab2;

import java.util.Scanner;

public class AverageGrades {

	public static void main(String[] args) {
		int done = -1;
		int count = 0;
		int grade = 0;
		int total = 0;
		
		Scanner input = new Scanner(System.in); // reading in user input
		System.out.print("Enter the first grade: ");
		grade = Integer.parseInt(input.nextLine());
		
		while (grade != done) {
			total += grade;
			count ++;
			
			System.out.print("Enter the next grade, or -1 when done. ");
			grade = Integer.parseInt(input.nextLine());
		}
		
		input.close();
		
		if (grade == done && count != 0) {
			int avg = total / count;
			System.out.println("Total number of grades enter is : " + count 
					+ ". "
					+ "The average grade is: " + avg + "\n");
		} else if (grade == done && count == 0) {
			System.out.println("No grade has been entered. Average is 0. ");
		}												
	
	}

}

// Test program with various input, works as expected.
/* 
 * Test 1: first grade = -1:
  		Enter the first grade: -1
 		No grade has been entered. Average is 0. 
 * Test 2:
 		Enter the first grade: 85
		Enter the next grade, or -1 when done. 73
		Enter the next grade, or -1 when done. 69
		Enter the next grade, or -1 when done. 66
		Enter the next grade, or -1 when done. -1
		Total number of grades enter is : 4. The average grade is: 73 
 */

