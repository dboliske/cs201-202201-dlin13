# Lab 4

## Total

-/30

## Break Down

* Part I GeoLocation
  * Exercise 1-9        8/8
  * Application Class   1/1
* Part II PhoneNumber
  * Exercise 1-9        8/8
  * Application Class   1/1
* Part III 
  * Exercise 1-8        8/8
  * Application Class   1/1
* Documentation         3/3

## Comments
1. Make toString output "(lat, lng)" format next time.
Well done!