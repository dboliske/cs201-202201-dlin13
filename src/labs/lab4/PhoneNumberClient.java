/* 
 * PhoneNumber class
 * This class is constructed following the UML Diagram of PhoneNumber
 * Donglei Lin， section 2
 */

package labs.lab4;

public class PhoneNumberClient {

	public static void main(String[] args) {
		//initiate an instance using the default constructor of PhoneNumber
		PhoneNumber ph1 = new PhoneNumber();
		System.out.println("System default phone number is: " + ph1.toString());
		
		//initiate an instance using the non-default constructor of PhoneNumber
		System.out.println("Overriding the default phone number...");
		PhoneNumber ph2 = new PhoneNumber("+1","312","234-5678");
		System.out.println("The first phone number is: " + ph2.toString());
		//check the length of area code and number
		System.out.println("Is the area code 3 digits long: " + ph2.aCodeCheck(ph2));
		System.out.println("Is the phone number 7 digits long: " + ph2.numCheck(ph2));
	
		//construct a 3rd PhoneNumber instance with different number
		PhoneNumber ph3 = new PhoneNumber("+1", "918", "876-5432");
		System.out.println("Anothe phone number is: " + ph3.toString());
		//compare ph2 and ph3
		System.out.println("Is this number equals to the previous one:? " + ph3.equals(ph2));
	}

}
