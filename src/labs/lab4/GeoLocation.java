/* 
 * GeoLocation class
 * This class is constructed following the UML Diagram of GeoLocation
 * Donglei Lin， section 2
 */

package labs.lab4;

public class GeoLocation {

	private double lat;
	private double lng;
	
	public GeoLocation() {
		lat = 0.0;
		lng = 0.0;
	}
	
	public GeoLocation(double latitude, double lngitude) {
		lat = latitude;
		lng = lngitude; 
	}
	
	public void setLat(double lat) {
		this.lat = lat;
	}
	 public void setLng(double lng) {
		 this.lng = lng;
	 }
	 
	 public double getLat() {
		 return lat;
	 }

	 public double getLng() {
		 return lng;
	 }
	 
	 public String toString() {
		 return "Your location is: \n "
		 		+ "Latitude: " + lat + "\n "
		 		+ "Longitude: " + lng;
	 }
	 
	 public boolean latRange(GeoLocation g) {
		 return (g.getLat() >= -90 && g.getLat() <= +90);
		 }
	 
	 public boolean lngRange(GeoLocation g) {
		 return (g.getLng() >= -180 && g.getLng() <= +180);
		 }
}
