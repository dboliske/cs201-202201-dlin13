/* 
 * PhoneNumber class
 * This class is constructed following the UML Diagram of PhoneNumber
 * Donglei Lin， section 2
 */

package labs.lab4;

public class PhoneNumber {
	
	private String countryCode;
	private String areaCode;
	private String number;
	
	public PhoneNumber() {
		countryCode = "+1";
		areaCode = "800";
		number = "000-0000";
	}

	public PhoneNumber(String cCode, String aCode, String num) {
		countryCode = cCode;
		areaCode = aCode;
		number = num;
	}
	
	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}
		
	public void setAreaCode(String areaCode) {
		this.areaCode = areaCode;
	}
	
	public void setNumber(String number) {
		this.number = number;
	}
	
	public String getCountryCode() {
		return countryCode;
	}
	
	public String getAreaCode() {
		return areaCode;
	}
	
	public String getNumber() {
		return number;
	}
	
	public String toString() {
		return "Phone number is: " + countryCode + "(" + areaCode + "-" + number + ")";
	}
	
	public boolean aCodeCheck(PhoneNumber n) {
		return n.getAreaCode().length() == 3;
	}
	
	public boolean numCheck(PhoneNumber n) {
		return n.getNumber().length() == 7;
	}
	
	public boolean equals(PhoneNumber n) {
		return this.countryCode.equals(n.getCountryCode()) && this.areaCode.equals(n.getAreaCode()) && this.number.equals(n.getNumber()) ;
	}
	
}
