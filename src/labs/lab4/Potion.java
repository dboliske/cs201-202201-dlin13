/* 
 * Potion class
 * This class is constructed following the UML Diagram of Potion
 * Donglei Lin， section 2
 */

package labs.lab4;

public class Potion {
	
	private String name;
	private double strength;
	
	public Potion() {
		name = "Magic";
		strength = 0;
		
	}
	
	public Potion(String n, double s) {
		name = n;
		strength = s;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public void setStrength(double strength) {
		this.strength = strength;
	}
	
	
	public String getName() {
		return name;
	}

	public double getStrength() {
		return strength;
	}

	public String toString() {
		return name + " with a strength of  " + strength;
	}
	
	public boolean strengthRange(Potion p) {
		return p.strength >= 0 && p.strength <= 10;
	}
	
	public boolean equals(Potion p) {
		if (this.name.equals(p.getName()) && this.strength != p.getStrength()) {
			System.out.println("Potion name is the same: " + this.name + " but strength is different: \n"
					           + this.strength + " vs. " + p.getStrength());
			return false;
		} else if (!this.name.equals(p.getName()) && this.strength == p.getStrength()) {
			System.out.println("Different name: " + this.name + " vs. " + p.getName() + "\n"
			                   + "with the same level of strength: " + this.strength);
			return false;
		}
		System.out.println("2 completely Potions.");
		return this.name.equals(p.getName()) && this.strength == p.getStrength();
	}
	
}
