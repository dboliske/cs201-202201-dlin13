package labs.lab4;

public class GeoLocationClient {

	public static void main(String[] args) {
		
		//initiate an instance using the default GeoLocation constructor 
		GeoLocation g1 = new GeoLocation();
		System.out.println(g1.toString());
		System.out.println("Is the latitude between -90 and +90? :" + g1.latRange(g1));
		System.out.println("Is the longitude between -180 and +180? :" + g1.lngRange(g1));
		System.out.println("Overriding default values of latitude and longitude...");
		//override the default values of the default constructor 
		g1.setLat(41.89004);
		System.out.println(g1.toString());
		g1.setLng(-87.62446);
		System.out.println(g1.toString());
		System.out.println("Is the latitude between -90 and +90? :" + g1.latRange(g1));
		System.out.println("Is the longitude between -180 and +180? :" + g1.lngRange(g1));
		
		//initiate a non-default constructor of GeoLocation instance
		System.out.println("Initiate the non-default construction of GeoLocation class: ");
		GeoLocation g2  = new GeoLocation(41.88989, -87.62577);
		System.out.println(g2.toString());
		System.out.println("Is the latitude between -90 and +90? :" + g2.latRange(g2));
		System.out.println("Is the longitude between -180 and +180? :" + g2.lngRange(g2));
		System.out.println("Overriding default values of latitude and longitude...");
		//override the initial values set on the non-default constructor
		g2.setLat(41.89004);
		System.out.println(g2.toString());
		g2.setLng(-87.62446);
		System.out.println(g2.toString());
		System.out.println("Is the latitude between -90 and +90? :" + g2.latRange(g2));
		System.out.println("Is the longitude between -180 and +180? :" + g2.lngRange(g2));
	}

}
