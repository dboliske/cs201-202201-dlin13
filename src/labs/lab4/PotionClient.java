package labs.lab4;

public class PotionClient {

	public static void main(String[] args) {
		// initiate the default Potion instance using the default constructor
		Potion p1 = new Potion();
		System.out.println("Default Potion is: " + p1.toString());
		
		// initiate a second Potion instance using the non-default constructor
		Potion p2 = new Potion("SweatSoup", 8);
		System.out.println("What do we get this time? \n" + p2.getName() + "\n"
		                   + p2.toString());
		// check if the strength of the new Potion falls in the range of (0,10)
		System.out.println("Does the strengh falls between 0 and 10? \n" + p2.strengthRange(p2));
		// compare p1 and p2
		System.out.println("Are they the same potion? \n" + p2.equals(p1));
		
		// initiate more instances
		Potion p3 = new Potion("SweatSoup", 4);
		Potion p4 = new Potion("Bland", 4);
		// compare p2, p3 and p4
		System.out.println("Potion #3: " + p3.toString());
		System.out.println("Potion #4: " + p4.toString());
		System.out.println("Is Potion #2 & #3 the same? " + p2.equals(p3));
		System.out.println("Is Potion #3 & #4 the same? " + p3.equals(p4));
		System.out.println("Is Potion #2 & #4 the same? " + p2.equals(p4));
	}

}
