/* Produce class
 * This Java program inherits the BaseItem class and is used to construct produce type items.
 * Donglei Lin
 * 4/14/2022
 */

package project;

public class Produce extends Item {
	
	private String expiry;
	
	public Produce() {
		super();
		this.expiry = null;
	}
	
	public Produce(String name, double price, double qty, String expiry) {
		super(name, price, qty);
		this.expiry = expiry;
	}

	public String getExpiry() {
		return expiry;
	}

	public void setExpiry(String expiry) {
		this.expiry = expiry;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (!super.equals(obj)) {
			return false;
		} else if (!(obj instanceof Produce)) {
			return false;
		}
		
		Produce p = (Produce)obj;
		if (this.expiry != p.getExpiry()) {
			return false;
		}
		return true;
	}
	
	@Override
	public String toString() {
		return super.toString() + " " + "best before: " + " " + expiry;
	}
	
	@Override
	public String toCSV() {
		return super.toCSV() + "," + expiry;
	}

}
