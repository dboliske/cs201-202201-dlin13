# Final Project

## Total

136/150

## Break Down

Phase 1:                                                            46/50

- Description of the user interface                                 3/3
- Description of the programmer's tasks:
  - Describe how you will read the input                            3/3
  - Describe how you will process the data from the input file      4/4
  - Describe how you will store the data                            3/3
  - How will you add/delete/modify data?                            5/5
  - How will you search data?                                       5/5
- Classes: List of names and descriptions                           7/7
- UML Class Diagrams                                                10/10
- Testing Plan                                                      6/10

Phase 2:                                                            90/100

- Compiles and runs with no run-time errors                         10/10
- Documentation                                                     15/15
- Test plan                                                         0/10
- Inheritance relationship                                          5/5
- Association relationship                                          5/5
- Searching works                                                   5/5
- Uses a list                                                       5/5
- Project reads data from a file                                    5/5
- Project writes data to a file                                     5/5
- Project adds, deletes, and modifies data stored in list           5/5
- Project generates paths between any two stations                  10/10
- Project encapsulates data                                         5/5
- Project is well coded with good design                            10/10
- All classes are complete (getters, setters, toString, equals...)  5/5

## Comments
For the testing plan, while both of these are true, there should be more clearly defined steps to determine whether each class is working properly and what occurs when invalid actions are attempted.
The stocks.csv file with real world prices is a good inclusion
### Design Comments
No specific testing plan included with scenarios to test
### Code Comments
