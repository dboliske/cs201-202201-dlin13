/* AgeRestricted class
 * This Java program inherits the BaseItem class and is used to construct age restricted type items.
 * Donglei Lin
 * 4/14/2022
 */

package project;

public class AgeRestricted extends Item {
	
	private int age;
	
	public AgeRestricted() {
		super();
		this.age = 21;
	}
	
	public AgeRestricted(String name, double price, double qty, int age) {
		super(name, price, qty);
		this.age = age;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public boolean equals(Object obj) {
		if (!super.equals(obj)) {
			return false;
		} else if (!(obj instanceof Produce)) {
			return false;
		}
		
		AgeRestricted ag = (AgeRestricted)obj;
		if (this.age != ag.getAge()) {
			return false;
		}
		return true;
	}
	
	@Override
	public String toString() {
		return super.toString() + " " + "minimum age: " + age + " . ID must be shown at checkout. ";
	}
	
	@Override
	public String toCSV() {
		return super.toCSV() + "," + age;
	}
}
