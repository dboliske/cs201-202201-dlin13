/* 
 * ShoppingCart class
 * This program models a shopping app for users/grocery customers to shop store items. It allows users to add and remove items from the shopping cart,
 * and simultaneously update the store stock accordingly (in an opposite direction).
 * Donglei Lin
 * 4/14/2022
 */

package project;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

public class ShoppingCart {

	private ArrayList<Item> shoppingCart; 
	
	public ShoppingCart() {
		shoppingCart = new ArrayList<Item>();
	}
	
	public void addItem(String name, double qty, ArrayList<Item> stock) {
		Item iCart = new Item(); 
		iCart.setName(name);
		iCart.setQty(qty);
		// if shopping cart is empty, requested amount will the total cart amount
		if (shoppingCart.isEmpty()) {
			for (Item s : stock) {
				if (s.getName().equalsIgnoreCase(name)) {
					iCart.setPrice(s.getPrice());
					shoppingCart.add(iCart);
				}
			}
		// if cart is not empty, check if the item has been previously added, such that the new total will be current amount + requested amount
		// otherwise new total is also the requested amount
		} else {
			boolean found = false;
			for (int c=0; c<shoppingCart.size(); c++) {
				if (shoppingCart.get(c).getName().equalsIgnoreCase(name)) {
					found = true;
					double curr = shoppingCart.get(c).getQty();
					shoppingCart.get(c).setQty(curr+qty);;	
					break;
				}
			
			}
			if (!found) {
				iCart.setQty(qty);
				shoppingCart.add(iCart);

			}
		}
		
		// last, subtract the amount from stock. if item gets sold out, quantity will be set to 0
		// such that when user browse store items it will show that the store did have the item before, but sold out at the moment
		for (Item s : stock) {
			if (s.getName().equalsIgnoreCase(name)) {
				double nQty = s.getQty() - qty;
				s.setQty(nQty);
				break;
			}
		}
	}

	
	public void removeItem(String name, double qty, ArrayList<Item> stock) {
		/* There are 2 procedures involved:
		 * 1. remove from shopping cart;
		 * 2. add the removed quantity back to store inventory;
		 */
		
		if (!shoppingCart.isEmpty()) {
			for (int c=0; c<shoppingCart.size(); c++) {
				if (shoppingCart.get(c).getName().equalsIgnoreCase(name)) {
					double currQty = shoppingCart.get(c).getQty();
					double newQty = (currQty - qty);
					if (newQty == 0) {
						shoppingCart.remove(c);
					} else if (newQty > 0){
						shoppingCart.get(c).setQty(newQty);
					} else if (newQty < 0) {
						System.out.println("Error removing: remove quantity greater than cart quantity. Please try again");
						break;
					}
					System.out.println(qty + " " + name + " has been removed from your cart.");
				}
			}
			// add items back to inventory
			for (int s=0; s<stock.size(); s++) {
				if (stock.get(s).getName().equalsIgnoreCase(name)) {
					double invQty = stock.get(s).getQty();
					stock.get(s).setQty(invQty+qty);
					break;
					}
				}
			
		} else {
			System.out.println("Your Cart is Empty. ");
		}
	
	}
	
	public void searchItem(String name, ArrayList<Item> stock) {
		boolean found = false;
		for (int i=0; i<stock.size(); i++) {
			if(stock.get(i).getName().equalsIgnoreCase(name)) {
				System.out.println("Item " + name + " found. " + stock.get(i).getQty() + " available.");
				found = true;
			} 
		}
		if (!found) {
			System.out.println("Item " + name + " not found. Please try again. ");
		}
	}
	
	public void viewCart() { 
		if (shoppingCart.isEmpty()) {
			System.out.println("Your Cart is Empty. ");
		} else {
			for (Item item: shoppingCart) {
				System.out.println(item.getName() + " qty: " + item.getQty() + " $" + item.getPrice() + " each.");
			}
		}
	}
	
	public void checkout() {
		double total = 0.0;
		for (Item item : shoppingCart) {
			System.out.println(item.getName() + " qty: " + item.getQty() + " $" + item.getPrice() + " each.");
			double subtotal = item.getPrice() * item.getQty();
			total =+ subtotal;
		}
		System.out.println("Total is: $" + total);
		
	}

	public ArrayList<Item> getShoppingCart() {
		return shoppingCart;
	}

	public void setShoppingCart(ArrayList<Item> shoppingCart) {
		this.shoppingCart = shoppingCart;
	}
	
}