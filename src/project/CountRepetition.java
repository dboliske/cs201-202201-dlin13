/* CountRepetition class
 * This Java program is to extract the distinct values in a list, array, array list etc. type of data, aggregates items by using count of repetition
 * to indicates the times of its appearance.
 * TODO:  (optional/for later) expand the class method to be able to handle other list type of data structure. 
 */

package project;

import java.util.ArrayList;
import java.util.Arrays;

public class CountRepetition {
	public static ArrayList<String> countFreq(ArrayList<String> arr, int n)
	{
	    boolean visited[] = new boolean[n];
	    ArrayList<String> newString = new ArrayList<String>(); 
	    Arrays.fill(visited, false);
	 
	    // Traverse through array elements and
	    // count frequencies
	    for (int i = 0; i < n-1; i++) {	    	
	        // Skip this element if already processed
	        if (visited[i] == true)
	            continue;
	        // Count frequency
	        int count = 1;
	        for (int j = i + 1; j < n; j++) {
	            if (arr.get(j).equals(arr.get(i))) {
	                visited[j] = true;
	                count++;
	            }
	        }
//	        System.out.println(arr.get(i) + " " + count);
	        newString.add(arr.get(i) + "," + Integer.toString(count));
	    }
	    return newString;
	}

}
