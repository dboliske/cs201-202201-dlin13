/* StoreApp class
 * This Java program models a shopping application for a grocery store.
 * 4/28/2022
 */

package project;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.Scanner;


public class StoreApp {
	
	public static ArrayList<Item> readFile (String filename) {
		ArrayList<Item> inStock = new ArrayList<Item> ();
		
		try {
			File f = new File(filename);
			Scanner input = new Scanner(f);
			
			// end-of-file loop
			ArrayList<String> temp = new ArrayList<String>();
			
			while (input.hasNextLine()) {
				try {
					String line = input.nextLine();
					temp.add(line);
				} catch (Exception e) {
					System.out.println(e);
				}
			}
			input.close();
			
			ArrayList<String> itemString = CountRepetition.countFreq(temp,temp.size());
			// create item based on the pattern of the last string
			String datePattern = "^\\d{2}/\\d{2}/\\d{4}$"; // date pattern, indicates best before date
			String agePattern  = "[0-9]{2}"; // integer pattern, indicates age
			
			for (String s:itemString) {
				String [] values = s.split(",");
				Item i = null;
				
				if (values.length == 3) {
					i = new Item(values[0], Double.parseDouble(values[1]), Double.parseDouble(values[2]));
				} else if (values.length == 4) {

					if (values[2].matches(datePattern)) {
						i = new Produce(values[0], Double.parseDouble(values[1]), Double.parseDouble(values[3]), values[2]);
					} else if (values[2].matches(agePattern)) {
						i = new AgeRestricted(values[0], Double.parseDouble(values[1]),Double.parseDouble(values[3]), Integer.parseInt(values[2]));	
					}
				} 
				inStock.add(i);	
			}
			}catch (FileNotFoundException fnf) {
				System.out.println(fnf);
			} catch(Exception e) {
				System.out.println("Error occurred reading in file.");
			}
			
		return inStock;
	}
	

	public static void startScreen() {
		
		System.out.println("Main Menu: \n");
		String[] options = {"Browse Items", "Search Items", "Add Item", "Remove Item", "Review Cart", "Check Out", "Exit"};
		for (int i=0; i<options.length; i++) {
			System.out.println((i+1) + ". " + options[i]);
		}
	}
	
	public static ArrayList<Item> menu(Scanner input, ArrayList<Item> invCopy) {
		boolean done = false;
		ShoppingCart cart = new ShoppingCart();
		System.out.println("Welcome! \n");	
		do {
			
			startScreen();
			String choice = getUserInput(input);
			
			switch (choice) {
			case "1":
				// print available items, starts new cart
				displayStoreItem(invCopy);
				break;
			case "2":
				// search item
				System.out.println("Enter item name: ");
				String name = input.nextLine();
				cart.searchItem(name, invCopy);
				break;
			case "3":
				// add item to cart
				System.out.println("Enter item name: ");
				name = input.nextLine();
				// check if user input is valid first
				if (!findItem(name, invCopy)) {
					System.out.println("No item found! Please try again. ");
				} else {
					System.out.println("Enter item quantity: ");
					double add = Double.parseDouble(input.nextLine());
					if (outOfStock(name, add,invCopy)) {
						System.out.println("Insufficient amount available. Please choose again. ");
					} else {
						cart.addItem(name, add, invCopy);	
					}
				}
				break;
			case "4":
				// remove item from cart
				System.out.println("Enter item name: ");
				name = input.nextLine();
				if (!findItem(name, invCopy)) {
					System.out.println("No item found! Please try again. ");
				} else {
					System.out.println("Enter item quantity: ");
					double del = Double.parseDouble(input.nextLine());
					cart.removeItem(name, del, invCopy);	
				}
				break;
			case "5":
				// review cart
				cart.viewCart();
				break;
			case "6":
				// check out cart
				cart.checkout();
				break;
			case "7":
				done = true;
				System.out.println("Thank you for shopping. Goodbye!");
				break;	
			default:
				System.out.println("That's not an option. Please try again.");
			} 
		} while (!done);
		
		return invCopy;
	}
	
	
	public static void displayStoreItem(ArrayList<Item> invCopy) {
		for (Item i : invCopy) {
			if (i.getQty()==0) { // if sold out will show item is out of stock
				System.out.println(i.getName() + " is out of stock.");
			} else {
				System.out.println(i.toString());	
			}
		}
	}
	
	private static String getUserInput(Scanner input) {
        String choice = input.nextLine();
        return choice;
    }
	
	// utility function to check if user input is valid
	private static boolean findItem(String s, ArrayList<Item> invCopy) {
		boolean found = false;
		for (Item i : invCopy) {
			if (i.getName().equalsIgnoreCase(s)) {
				found = true;
			} 
		} 
		return found;
	}
	
	// utility function to check is the requested item is in stock or sold out.
	private static boolean outOfStock(String s, double qty, ArrayList<Item> invCopy) {
		boolean outOfStock = true;
		for (Item i: invCopy) {
			if (i.getName().equalsIgnoreCase(s) && qty <= i.getQty()) {
				outOfStock = false;
			}
		}
		return outOfStock;
	}
	
	public static void saveFile(String filename, ArrayList<Item> invCopy) {
		try {
			FileWriter writer = new FileWriter(filename);
			
			for (Item i : invCopy) {
				String csvLine = i.toCSV();
				int reps = (int) i.getQty();
				for (int r=0; r<reps; r++)
					writer.write(csvLine + "\n");
					writer.flush();
			}
			writer.close();
		} catch (Exception e) {
			System.out.println("Error saving to file.");
		}
	}
	
	public static void main(String[] args) throws FileNotFoundException {
		Scanner input = new Scanner(System.in);
		// load inventory from stock.csv file
		// keep the original, copied as stock2
		ArrayList<Item> inventory = readFile("src/project/stock2.csv");
		
		ArrayList<Item> invCopy = new ArrayList<Item> ();
		invCopy.addAll(inventory);
		invCopy = menu(input, inventory);
		
		// write updated inventory back to .csv file
		saveFile("src/project/stock2.csv", invCopy);
		input.close();		
	}	

}
