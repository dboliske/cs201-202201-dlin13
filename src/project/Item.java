/*
 * Item class
 * Single item object constructor
 * Donglei Lin
 * 4/14/2022
 */

package project;

public class Item {
	
	private String name;
	private double price;
	private double qty;
	
	public Item() {
		this.name = null;
		this.price = 0.0;
		this.qty = 0.0;
				
	}
	
	public Item(String name, double price, double qty) {
		this.name = name;
		this.price = price;
		this.qty = qty;
	}


	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}
	
	public double getQty() {
		return qty;
	}

	public void setQty(double d) {
		this.qty = d;
	}

	
	@Override
	public boolean equals(Object obj) {
		if (obj == null) {
			return false;
		} else if (this == obj) {
			return true;
		} else if (!(obj instanceof Item)) {
			return false;
		}
		
		Item i = (Item)obj;
		if (!this.name.equals(i.getName())) {
			return false;
		} else if (this.name != i.getName()) {
			return false;
		} else if (this.price != i.getPrice()) {
			return false;
		} else if (this.qty != i.getQty()) {
			return false;
		}
		
		return true;
	}
	
	@Override
	public String toString() {
		return name + " ,$" + price + " " + qty + " . available. " ;
	}
	
	public String toCSV() {
		return name + "," + price;
	}


}


